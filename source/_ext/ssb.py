from docutils import nodes
from docutils.parsers import rst
from sphinx.util.docutils import SphinxDirective

class VideoNode(nodes.Element): pass

class VideoDirective(rst.Directive):
    required_arguments=1
    option_spec={"frame":str}
    has_content=False

    def run(self):
        #print(text)
        src=f"https://www.youtube.com/embed/{self.arguments[0]}?feature=player_detailpage"
        return [
            #nodes.paragraph(text=f"VideoDirective: args={self.arguments}, options={self.options!r}"),
            nodes.raw(
                text=f"<iframe width=640 height=360 src='{src}'></iframe>",
                format="html",
            )
        ]


def setup(app):
    app.add_directive("video", VideoDirective)

    return {
        "version" : 0.1,
        'parallel_read_safe': True,
        'parallel_write_safe': True,
    }
    
